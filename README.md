# Training-spring-security

## Dependencies
* h2
* spring-boot-starter-web
* spring-boot-starter-data-jpa
* spring-boot-starter-security

## Clone the project
```sh
git clone https://gitlab.com/training741/training-spring-security.git
```

## Run the project 
```sh
./mvnw spring-boot:run

```
