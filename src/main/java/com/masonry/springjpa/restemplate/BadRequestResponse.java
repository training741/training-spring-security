package com.masonry.springjpa.restemplate;

public class BadRequestResponse extends RuntimeException{

    private final ErrorResponse error;

    public BadRequestResponse(ErrorResponse error) {
        super(error.getMessage());
        this.error = error;
    }

    public ErrorResponse getError() {
        return error;
    }
}
