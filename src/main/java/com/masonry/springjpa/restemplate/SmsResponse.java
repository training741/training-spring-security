package com.masonry.springjpa.restemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmsResponse {

    public SmsResponse(String message) {
        this.message = message;
    }

    private int smsSize=1;
    private int contacts=1;
    private String message;

    @Override
    public String toString() {
        return "SmsResponse{" +
                "smsSize=" + smsSize +
                ", contacts=" + contacts +
                ", message='" + message + '\'' +
                '}';
    }
}
