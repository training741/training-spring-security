package com.masonry.springjpa.restemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sms {
    private String sender="";
    private String text="";
    private String number="";

    public boolean isInvalid() {
        return sender.length() == 1 || number.length()== 1 ;
    }

    @Override
    public String toString() {
        return "Sms{" +
                "sender='" + sender + '\'' +
                ", text='" + text + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
