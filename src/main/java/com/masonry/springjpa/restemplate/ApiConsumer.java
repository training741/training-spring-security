package com.masonry.springjpa.restemplate;

import org.apache.coyote.Response;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.function.ServerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ApiConsumer {
    private final WebClient webClient;

    public static void main(String... args){
        try{
            ApiConsumer apiConsumer = new ApiConsumer(WebClient.builder());
            apiConsumer.postHttpRequest();
        }catch (Exception e){
            ErrorResponse response;
            if(e instanceof BadRequestResponse ) {
                response = ((BadRequestResponse) e).getError();
                System.out.println("***************-----########## "+response.getMessage());
            }

        }

        //apiConsumer.callGetApi();

    }



    public ApiConsumer(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder
                .baseUrl("http://localhost:8080")
                .defaultHeader(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public void callGetApi(){
        Mono<SmsResponse> mono = webClient.get().uri("/sms/2")
                .retrieve()
                .bodyToMono(SmsResponse.class);
        System.out.println("################## executing request ####################################");
        System.out.println(mono.block().toString());
    }

    public void postHttpRequest(){
        Mono<SmsResponse> mono2 = webClient.post().uri("/sms")
                .retrieve()/*.onStatus(HttpStatus.BAD_REQUEST::equals,
                        response -> response.bodyToMono(response.).map(BadRequestResponse::new))*/
                        .bodyToMono(SmsResponse.class);

        System.out.println("########: --"+ mono2.block().toString());


    }
}
