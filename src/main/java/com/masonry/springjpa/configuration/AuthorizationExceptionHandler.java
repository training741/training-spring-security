package com.masonry.springjpa.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

public class AuthorizationExceptionHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response
            , AccessDeniedException accessDeniedException) throws IOException, ServletException {
        Principal user = request.getUserPrincipal();
        //response body*************
        Map<String, Object> map = new HashMap<>();
        map.put("timestamp", System.currentTimeMillis());
        map.put("status", HttpServletResponse.SC_FORBIDDEN);
        map.put("message", "authorization failure");
        map.put("path", request.getRequestURI());
        //************************
        response.setContentType("application/json");
        response.setHeader("x-message",accessDeniedException.getMessage());
        if(user != null) {
            response.setHeader("x-user", user.getName());
            response.setHeader("x-user-representaion", ((UsernamePasswordAuthenticationToken) user).toString());
        }
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), map);
        } catch (Exception e) {
            throw new IOException();
        }

    }
}
