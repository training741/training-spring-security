package com.masonry.springjpa.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

public class AuthenticationExceptionHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response
            , AuthenticationException authException) throws IOException, ServletException {
       Principal user = request.getUserPrincipal();

        //response body*************
        Map<String, Object> map = new HashMap<>();
        map.put("timestamp", System.currentTimeMillis());
        map.put("status", HttpServletResponse.SC_UNAUTHORIZED);
        map.put("message", "authentication failure");
        map.put("path", request.getContextPath());
        //************************
        response.setContentType("application/json");
        response.setHeader("x-message",authException.getMessage());
        response.setHeader("x-user", request.getRemoteUser());
        if(user != null) {
            response.setHeader("x-user", user.getName());
            response.setHeader("x-user-representaion", ((UsernamePasswordAuthenticationToken) user).toString());
        }
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), map);
        } catch (Exception e) {
            throw new IOException();
        }

    }
}
