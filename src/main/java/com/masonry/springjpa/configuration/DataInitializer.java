package com.masonry.springjpa.configuration;

import com.masonry.springjpa.repository.Task;
import com.masonry.springjpa.repository.TaskRepository;
import com.masonry.springjpa.repository.user.User;
import com.masonry.springjpa.repository.user.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class DataInitializer {

   @Bean
    CommandLineRunner intializer(UserRepository userRepository, PasswordEncoder encoder) {
        return args -> {
            String password = encoder.encode("password");
            userRepository.save(new User("fernand",password,"ROLE_ADMIN,ROLE_USER"));
            userRepository.save(new User("mason",password,"ROLE_USER"));
            userRepository.save(new User("dummy",password,"ROLE_REPORTER"));
        };

    };
}
