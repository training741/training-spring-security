package com.masonry.springjpa.controller;

import com.masonry.springjpa.repository.Task;
import com.masonry.springjpa.repository.TaskRepository;
import org.apache.coyote.Response;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TaskController {
    private final TaskRepository taskRepository;

    public TaskController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }
    @GetMapping("/public")
    public String publicMessage(){
       return "dummy public api";
    }

    @PostMapping("/public")
    public String publicMessage2(){
        return "dummy POST public api";
    }
    @GetMapping("/api")
    public String dummyGetApi(){
        return "dummy GET api";
    }


    //@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_REPORTER)")
    @Secured({"ROLE_USER","ROLE_REPORTER"})
    @PostMapping("/api")
    public String dummyPostApi(){
        return "dummy POST api";
    }

    //@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/tasks")
    public ResponseEntity<List<Task>> getAllTasks(@RequestParam(defaultValue = "0", name = "page") int pageNumber
            ,@RequestParam(defaultValue = "10")int size){
        Pageable pageable = PageRequest.of(pageNumber,size);

          Page<Task>page= taskRepository.findAll(pageable);
        if(page.isEmpty())
            return ResponseEntity.noContent().build();

        return ResponseEntity
                .ok().headers(buildHeaders(page))
                .body(page.getContent());
    }

    public HttpHeaders buildHeaders(Page<?> page) {
        Map<String,String> keyValueMap = new HashMap<>();
        keyValueMap.put("current-page", String.valueOf(page.getNumber()));
        keyValueMap.put("total-pages", String.valueOf(page.getTotalPages()));
        keyValueMap.put("total-elements", String.valueOf(page.getTotalElements()));
        HttpHeaders headers = new HttpHeaders();
        headers.setAll(keyValueMap);
        return headers;
    }
}
