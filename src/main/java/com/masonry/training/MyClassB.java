package com.masonry.training;

public class MyClassB implements Cloneable,MyInterface {
    private int varOne;
    private String name;

    public MyClassB(int varOne, String name) {
        this.varOne = varOne;
        this.name = name;
    }

    public int getVarOne() {
        return varOne;
    }

    public void setVarOne(int varOne) {
        this.varOne = varOne;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MyClassB{" +
                "varOne=" + varOne +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public void methodOne() {

    }

    @Override
    public int calculate(int a, int b) {
        return 0;
    }

}
