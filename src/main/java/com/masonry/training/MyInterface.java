package com.masonry.training;

public interface MyInterface {
    int var = 12;
    void methodOne();
    int calculate(int a, int b);

    default void anotherMethod(){
        System.out.println("Default behavior");
    }

}
